# build.py


import argparse
import os


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('forwhat')
    args = parser.parse_args()
    if args.forwhat == 'use':
        srcdir = os.getcwd()
        os.system('mkdir -p for_use')
        os.chdir('for_use')
        env = ['PY2APP_FLAGS=-O2',
               'SRCDIR={}'.format(srcdir)]
        os.system('{} make -f {}/Makefile all'.format(' '.join(env), srcdir))


if __name__ == '__main__':
    main()

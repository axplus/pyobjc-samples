# AppController.py


import urllib2 as urllib
# from xml import parseString  # import xml.etree.cElementTree as etree
import objc
from Foundation import *


class AppController(NSObject):
    """docstring for AppController"""
    textField = objc.IBOutlet()
    readRepeat = objc.IBOutlet()
    results = {}
    # result = objc.IBOutlet()

    @objc.IBAction
    def search_(self, sender):
        word = self.textField.stringValue()
        # NSLog('call iciba.com to search {}'.format(word))  # ####
        # http://dict-co.iciba.com/api/dictionary.php?w=$userSearch
        iciba = urllib.urlopen('http://dict-co.iciba.com/api/dictionary.php?w={}'.format(word))
        self.results = NSDictionary.dictionaryWithDictionary_(xml_to_dict(iciba))
        iciba.close()


def xml_to_dict(xml_stream):
    import xml.etree.cElementTree as etree
    tree = etree.iterparse(xml_stream)
    d = {}
    for _, item in tree:
        d[item.tag] = item.text
    return d


# def node_to_dict(tree):
#     d = {}
#     for item in tree:
#         print item  # ###
#         try:
#             d[item.tag] = node_to_dict(item)
#         except:
#             d[item.tag] = item.text # 'xxx'  # #### node_to_dict(items)
#     return d

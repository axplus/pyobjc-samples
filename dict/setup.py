from distutils.core import setup
import py2app


setup(name='PyDict',
      app=['main.py'],
      data_files=['Application.xib'],
      options={'py2app': {'plist': {'NSMainNibFile': 'Application'}}})
